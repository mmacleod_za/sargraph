#!/usr/bin/env python3

#
# (c) 2019-2023 Antmicro <www.antmicro.com>
# License: Apache-2.0
#

import argparse
import sys
import time

import graph
import watch

from common import *

# Declare and parse command line flags
parser = argparse.ArgumentParser()
parser.add_argument('session',  metavar='SESSION-NAME', type=str, nargs='?', default=None,                      help='sargraph session name')
parser.add_argument('command',  metavar='COMMAND',      type=str, nargs='*',                                    help='send command')
parser.add_argument('-L',       metavar='LOGFILE-NAME', type=str, nargs='?', default=None,      dest='logfile', help='program logfile name')
parser.add_argument('-f',       metavar='DEVICE-NAME',  type=str, nargs='?', default=None,      dest='fsdev',   help='observe a chosen filesystem')
parser.add_argument('-m',       metavar='MOUNT-DIR',    type=str, nargs='?', default=None,      dest='fspath',  help='observe a chosen filesystem')
parser.add_argument('-n',       metavar='IFACE-NAME',   type=str, nargs='?', default=None,      dest='iface',   help='observe chosen network iface')
parser.add_argument('-o',       metavar='OUTPUT-NAME',  type=str, nargs='?', default='data',    dest='name',    help='set output base names')
parser.add_argument('-t',       metavar='TMPFS-COLOR',  type=str, nargs='?', default='#f2c71b', dest='tmpfs',   help='set tmpfs plot color' )
parser.add_argument('-c',       metavar='CACHE-COLOR',  type=str, nargs='?', default='#ee7af0', dest='cache',   help='set cache plot color' )
args = parser.parse_args()

def send(sid, msg):
    p = subprocess.Popen(["screen", "-S", sid, "-X", "stuff", f"{msg}\n"])
    while p.poll() is None:
        time.sleep(0.1)

# Check if sar is available
p = run_or_fail("sar", "-V", stdout=subprocess.PIPE)

# Check if screen is available
p = run_or_fail("screen", "-v", stdout=subprocess.PIPE)
version = scan("Screen version (\d+)", int, p.stdout.readline().decode())
if version is None:
    fail("'screen' tool returned unknown output")

# If the script was run with no parameters, run in background and gather data
if args.session is None:
    # Find requested disk device
    if args.fspath:
        args.fspath = os.path.realpath(args.fspath)
        with open("/proc/self/mounts", "r") as f:
            while args.fsdev is None:
                args.fsdev = scan(f"^(/dev/\S+)\s+{re.escape(args.fspath)}\s+", str, f.readline())
        if not args.fsdev:
            fail(f"no device is mounted on {args.fspath}")

    watch.watch(args.name, args.fsdev, args.iface, args.tmpfs, args.cache)
    sys.exit(0)

# Now handle the commands

# Check if a command was provided
if len(args.command) <= 0:
    fail("command not provided")

# Get session name and command name
sid = args.session
cmd = args.command
log_file = args.logfile

def command_start():
    print(f"Starting sargraph session '{sid}'")

    # Spawn watcher process, *sys.argv[3:] is all arguments after 'chart start' + '-o [log name]' if not given
    if "-o" not in sys.argv:
        sys.argv += ["-o", sid]
    p = subprocess.Popen(["screen", "-Logfile", f"{sid}.log", "-dmSL", sid, os.path.realpath(__file__), *sys.argv[3:]])

    while p.poll() is None:
        time.sleep(0.1)
    gpid = 0
    j = 0
    time.sleep(1)
    print(f"Session '{sid}' started")

def command_stop():
    print(f"Terminating sargraph session '{sid}'")

    try:
        gpid = int(os.popen(f"screen -ls | grep '.{sid}' | tr -d ' \t' | cut -f 1 -d '.'").read())
    except:
        print("Warning: cannot find pid.")
        gpid = -1
    if len(cmd) < 2:
        send(sid, "command:q:")
    else:
        send(sid, f"command:q:{cmd[1]}")
    if gpid == -1:
        print("Waiting 3 seconds.")
        time.sleep(3)
    else:
        while pid_running(gpid):
            time.sleep(0.25)

def command_label():
    # Check if the label name was provided
    if len(cmd) < 2:
        fail("label command requires an additional parameter")
    print(f"Adding label '{cmd[1]}' to sargraph session '{sid}'.")
    send(sid, f"label:{cmd[1]}")

duplicate_count=0
last=""
def send_loglabel(line):
    global duplicate_count
    global last
    if last == line:
        duplicate_count += 1
        send(sid, f"label: ..{duplicate_count}")
    else:
        duplicate_count = 0
        send(sid, f"label: {line}")
    last = line

def command_loglabel():
    with open(log_file, 'r') as file:
        for line in follow(file):
            line_without_timestamp = line[28:].strip()

            if line_without_timestamp.startswith("Reordering "):
                send_loglabel("Reorder")
            elif line_without_timestamp == "Reordering: 0%....10%....20%....30%....40%....50%....60%....70%....80%....90%....100%":
                send_loglabel("End reorder")
            elif line_without_timestamp == "<<<< Start make PSFs":
                send_loglabel("Image PSF")
            elif line_without_timestamp == "Stitching facets onto full image..":
                send_loglabel("Stitch facets")
            elif line_without_timestamp == "Clipping model image into facets...":
                send_loglabel("Clip model->facets")
            elif line_without_timestamp == "Writing dirty image..":
                send_loglabel("write dirty img")
            elif line_without_timestamp == "<<<< Start first inversions":
                send_loglabel("First inversions")
            elif line_without_timestamp == "<<<< Start major iterations":
                send_loglabel("Major iterations")
            elif line_without_timestamp == "<<<< Done major iterations":
                send_loglabel("End major")
            elif line_without_timestamp.startswith("== Deconvolving"):
                send_loglabel("Deconvolving")
            elif line_without_timestamp.startswith("== Converting model image to visibilities"):
                send_loglabel("model->vis")
            elif line_without_timestamp.startswith("Predicting"):
                send_loglabel("predict chunk")
            elif line_without_timestamp.startswith("== Constructing image =="):
                send_loglabel("construct image")
            elif line_without_timestamp.startswith("Gridding"):
                send_loglabel("grid chunk")




def command_save():
    print(f"Saving graph from session '{sid}'.")
    if len(cmd) < 2:
        send(sid, "command:s:")
    else:
        send(sid, f"command:s:{cmd[1]}")

def command_plot():
    if len(cmd) < 2:
        graph.graph(sid, args.tmpfs, args.cache)
    else:
        graph.graph(sid, args.tmpfs, args.cache, cmd[1])

if cmd[0] == "start":
    command_start()
elif cmd[0] == "stop":
    command_stop()
elif cmd[0] == "label":
    command_label()
elif cmd[0] == "loglabel":
    command_loglabel()
elif cmd[0] == 'save':
    command_save()
elif cmd[0] == 'plot':
    command_plot()
else:
    fail(f"unknown command '{cmd[0]}'")
